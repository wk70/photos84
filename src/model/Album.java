package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
/**
 * 
 * @author Matt Irving & Will Knox
 *
 */
public class Album implements Serializable{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6683147490098740935L;
	private ArrayList<Photo> photos;
	private String name;
	/**
	 * Constructor for an album object
	 * @param name Name of album
	 */
	public Album(String name) {
		this.name = name;
		photos = new ArrayList<Photo>();
	}
	
	/**
	 * Gets name of Album
	 * @return name of Album
	 */
	public String getName() {
		return name;
	}
	/**
	 * Sets name of album
	 * @param name Album's name becomes name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Returns photos associated with album
	 * @return ArrayList of type Photo containing associated photos
	 */
	public ArrayList<Photo> getPhotos(){
		return photos;
	}
	/**
	 * Returns number of photos contained within an album
	 * @return {@code int} representing number of photos in an album
	 */
	public int getPhotoNum() {
		return photos.size();
	}
	/**
	 * Returns a {@code Date[]} array with the oldest and newest photo dates
	 * @return {@code vals[2]}, where {@code vals[0]} is the oldest date, and {@code vals[1]} is the newest date
	 */
	public Date[] dateRange() {
		ArrayList<Photo> temp = new ArrayList<Photo>();
		temp.addAll(photos);
		Collections.sort(temp);
		Date[] vals = {temp.get(0).getDate().getTime(), temp.get(getPhotoNum()-1).getDate().getTime()};
		return vals;
	}
	/**
	 * Checks if two albums share the same name
	 * @param album other album to check with
	 * @return Returns true if both specified album objects share the same name
	 */
	public boolean equals(Album album) {
		return name.equals(album.name);
	}
	/**
	 * Converts album to string containing its info
	 * @return {@code name}
	 */
	public String toString() {
		return name;
	}
}
