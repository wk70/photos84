package model;
/**
 * @author Matt Irving & Will Knox
 */

import java.io.Serializable;

public class Tag implements Serializable {

	private static final long serialVersionUID = -381567628142179116L;
	private String name, val;
	
	/**
	 * Creates Tag 
	 * @param name Name of tag
	 * @param val Value of tag
	 */
	public Tag(String name, String val) {
		this.name = name;
		this.val = val;
	}
	
	/**
	 * Get name of tag
	 * @return Name of tag
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Get value of tag
	 * @return Value of tag
	 */
	public String getVal()
	{
		return val;
	}
	
	/**
	 * Returns name and value of tag
	 * @return Printed version of tag's name & value, "Test|5"
	 */
	public String toString(){
		return name + "|" + val;
	}
	/**
	 * Checks whether or not two tags are identical
	 * @param tag Other tag to compare to
	 * @return True if tags are identical (name and value are equivalent), false if not
	 */
	public boolean equals(Tag tag) {
		return name.equals(tag.name) && val.equals(tag.val); 
	}
}
