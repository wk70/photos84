package model;
/**
 * @author Matt Irving & Will Knox
 */
import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5089696479328930040L;
	private ArrayList<Album> albums;
	private String username;
	
	/**
	 * Constructor for User object
	 * @param username Username for user
	 */
	public User(String username) {
		this.username = username;
		albums = new ArrayList<Album>();
	}
	/**
	 * Returns username
	 * @return username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * returns ArrayList of associated Albums
	 * @return ArrayList of albums
	 */
	public ArrayList<Album> getAlbums(){
		return albums;
	}
	/**
	 * adds album to a user
	 * @param a
	 */
	public void addAlbum(Album a) {
		albums.add(a);
	}
	/**
	 * Removes album from a user
	 * @param a
	 */
	public void removeAlbum(Album a) {
		albums.remove(a);
	}
	/**
	 * Checks if two users share the same name
	 * @param user Specified other user to check with
	 * @return True if both users share the same username
	 */
	public boolean equals(User user) {
		return this.username.equals(user.username);
	}
}
