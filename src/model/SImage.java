package model;

import java.io.Serializable;
import javafx.scene.image.*;
/**
 * 
 * @author Matt Irving & Will Knox
 *
 */
public class SImage implements Serializable {
	
	
	
	private static final long serialVersionUID = -8495368115232317804L;
	private int width, height;
	private int[][] pix;
	
	/**
	 * Constructor makes JavaFX Image into a custom Serializable SImage object
	 * @param image JavaFX Image Object
	 */
	public SImage(Image image) {
		width = (int)image.getWidth();
		height = (int)image.getHeight();
		
		pix = new int[width][height];
		
		PixelReader preader = image.getPixelReader();
		for(int currWidth = 0; currWidth < width; currWidth++) {
			for(int currHeight = 0; currHeight < height; currHeight++) {
				pix[currWidth][currHeight] = preader.getArgb(currWidth, currHeight);
			}
		}
	}
	
	/**
	 * Returns SImage as JavaFX Image object
	 * @return JavaFX Image object
	 */
	public Image getImage()
	{
		WritableImage img = new WritableImage(width, height);
		
		PixelWriter pw = img.getPixelWriter();
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				pw.setArgb(i, j, pix[i][j]);
			}
		}
		
		return img;
	}
	/**
	 * Gets width of image
	 * @return Image width
	 */
	public int getWidth() {
		return width;
	}
	/**
	 * Gets height of image
	 * @return Image height
	 */
	public int getHeight() {
		return height;
	}
	/**
	 * Gets 2D int array of Image pixels
	 * @return 2D array of image pixels
	 */
	public int[][] getPixels(){
		return pix;
	}
	/**
	 * Check if two images are equivalent
	 * @param image Image to check equivalency with
	 * @return Returns true if images are equivalent (same size and pixel values), false otherwise
	 */
	public boolean equals(SImage image) {
		if(width != image.getWidth() || height != image.getHeight()) 
			return false;
		
		
		for(int currR = 0; currR < width; currR++) {
			for(int currC = 0; currC < height; currC++) {
				if(pix[currR][currC] != image.getPixels()[currR][currC])
					return false;
			}
		}
		
		return true;
	}
}
