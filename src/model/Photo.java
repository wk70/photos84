package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import javafx.scene.image.Image;

/**
 * 
 * @author Matt Irving & Will Knox
 *
 */
public class Photo implements Serializable, Comparable<Photo> {
	
	
	
	private static final long serialVersionUID = -4013482946084748382L;
	//static final long serialVersionUID = 0;
	private ArrayList<Tag> tags;
	private String name, caption;
	private Calendar date;
	private SImage image;
	
	/**
	 * 1st Constructor
	 * @param name Name of photo
	 * @param image SImage to be represented
	 * @param date Date image was modified
	 */
	public Photo(String name, SImage image, Calendar date) {
		this.name = name;
		this.image = image;
		this.tags = new ArrayList<Tag>();
		this.caption = "";
		
		this.date = date;
		this.date.set(Calendar.MILLISECOND, 0);
		
	}
	/**
	 * 2nd Constructor
	 * @param name Name of photo
	 * @param image Image to be represented (and transformed into SImage object)
	 * @param date Date image was modified
	 */
	public Photo(String name, Image image, Calendar date) {
		this.name = name;
		this.image = new SImage(image);
		this.tags = new ArrayList<Tag>();
		this.caption = "";
		
		this.date = date;
		this.date.set(Calendar.MILLISECOND, 0);
	}
	/**
	 * @return the tags
	 */
	public ArrayList<Tag> getTags() {
		return tags;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the caption
	 */
	public String getCaption() {
		return caption;
	}
	/**
	 * @param caption the caption to set
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	/**
	 * 
	 * @return Date photo was last modified
	 */
	public Calendar getDate() {
		return date;
	}
	/**
	 * Compares whether or not photo is a duplicate
	 * @param photo Other photo to compare to
	 * @return True if photo names match eachother, false otherwise
	 */
	public boolean equals(Photo photo) {
		return this.name.equals(photo.name);
	}
	/**
	 * Returns image contained in photo object
	 * @return returns image contained in photo object
	 */
	public Image getImage() {
		return image.getImage();
	}
	@Override
	/**
	 * Is used to sort lists of Photos by date
	 * @param o
	 */
	public int compareTo(Photo o) {
		// TODO Auto-generated method stub
		long compareDate = o.getDate().getTimeInMillis();
		return (int) (getDate().getTimeInMillis() - compareDate);
	}
	
}
