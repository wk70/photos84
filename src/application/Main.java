package application;
/**
 * @author Matt Irving & Will Knox
 */
import controller.LoginController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;


public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/loginFXML.fxml"));
			VBox root = loader.load();
			Scene scene = new Scene(root);
			
			LoginController controller = loader.<LoginController>getController();
			
			primaryStage.setScene(scene);
			primaryStage.setTitle("Photo Library");
			primaryStage.show();
			
			
			controller.start(primaryStage);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
