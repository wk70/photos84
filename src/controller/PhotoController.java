package controller;
/**
 * 
 * @author Matt Irving & Will Knox
 *
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import model.*;

public class PhotoController {
	@FXML
	private Text albumNameText;
	
	@FXML
	private TextField searchField;
	@FXML
	private Button goButton;
	
	@FXML
	private Button leftArrow, rightArrow;
	
	@FXML
	private Button logoutButton;
	@FXML
	private Button quitButton;
	
	@FXML
	private Button addButton;
	@FXML
	private Button deleteButton;
	@FXML
	private Button copyButton;
	@FXML
	private Button moveButton;
	
	@FXML
	private GridPane photoGrid;
	
	@FXML
	private ImageView imagePreview;
	
	@FXML
	private TextField captionField;
	@FXML
	private Button captionButton;
	
	@FXML
	private Text dateText;
	@FXML
	private ComboBox tagList;
	@FXML
	private TextField newTagType;
	@FXML
	private TextField tagName;
	@FXML
	private Button createTagButton;
	@FXML
	private Button deleteTagButton;
	
	@FXML
	private TableView tagTable;
	
	ArrayList<Photo> photos;
	ArrayList<Album> userAlb;
	
	Album selectedAlbum;
	
	Album searchAlbum;
	int searchPos;
	
	User user;
	String dataPath;
	int alPos;
	int slideshowPos = 0;
	
	/**
	 * Start scene method
	 * @param stage
	 */
	@SuppressWarnings("unchecked")
	public void start(User user, Album selectedAlbum) {
		this.user = user;
		this.selectedAlbum = selectedAlbum;
		//photos = selectedAlbum.getPhotos();
		dataPath = "src\\data\\userData\\" + user.getUsername() + ".dat";
		try {
			albumNameText.setText(selectedAlbum.getName());
			photoGrid.getChildren().clear();
			
			FileInputStream fIS = new FileInputStream(dataPath);
			ObjectInputStream oIS = new ObjectInputStream(fIS);
			
			userAlb = (ArrayList<Album>) oIS.readObject();
			for(Album al : userAlb) {
				if(selectedAlbum.equals(al))
				{
					photos = al.getPhotos();
					alPos = userAlb.indexOf(al);
				}
				
				
			}
			
			
			
			
		} catch (Exception error) {
			error.printStackTrace();
		}
		
	}
	/**
	 * Handles adding photos to an album
	 * @param event
	 */
	public void handleAdd(Event event) {
		try {
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Select Image");
		chooser.getExtensionFilters().addAll(
				new ExtensionFilter("Image Files", "*.bmp", "*.BMP", "*.gif", "*.GIF", "*.jpg", "*.JPG", "*.png",
						"*.PNG"),
				new ExtensionFilter("Bitmap Files", "*.bmp", "*.BMP"),
				new ExtensionFilter("GIF Files", "*.gif", "*.GIF"), new ExtensionFilter("JPEG Files", "*.jpg", "*.JPG"),
				new ExtensionFilter("PNG Files", "*.png", "*.PNG"));
		File selectedFile = chooser.showOpenDialog(null);
		
		if(selectedFile != null) {
			Image image = new Image(selectedFile.toURI().toString());
			String name = selectedFile.getName();
			Calendar date = Calendar.getInstance();
			date.setTimeInMillis(selectedFile.lastModified());
			Photo newPhoto = new Photo(name, image, date);
			
			for(Photo currP : photos) {
				if(currP.equals(newPhoto)) {
					Alert a = new Alert(AlertType.ERROR);
					a.setTitle("Album Error");
					a.setHeaderText("Photo Add Error");
					a.setContentText("A photo with the same name already exists, please try again");
					a.showAndWait();
					return;
				}
			}
			
			userAlb.get(alPos).getPhotos().add(newPhoto);
			
			writeData(userAlb);
			update();
		}
		} catch (Exception error){
			error.printStackTrace();
		}
	}
	/**
	 * Handles copying photos to other albums
	 * @param event
	 */
	public void handleCopy(Event event) {
		List<String> alNames = new ArrayList<>();
		for(Album als : userAlb) {
			alNames.add(als.getName());
		}
		
		ChoiceDialog<String> dialog = new ChoiceDialog<>("Please select an option", alNames);
		dialog.setTitle("Album Selection");
		dialog.setHeaderText("Please select the album to copy the photo to: ");
		dialog.setContentText("Album: ");
		
		Optional<String> result = dialog.showAndWait();
		
		int tarAl = alNames.indexOf(result.get());
		Photo selected = userAlb.get(alPos).getPhotos().get(slideshowPos);
		userAlb.get(tarAl).getPhotos().add(selected);
		
		writeData(userAlb);
		update();
		updateManual();
	}
	/**
	 * Handles moving photos between albums
	 * @param event
	 */
	public void handleMove(Event event) {
		List<String> alNames = new ArrayList<>();
		for(Album als : userAlb) {
			alNames.add(als.getName());
		}
		
		ChoiceDialog<String> dialog = new ChoiceDialog<>("Please select an option", alNames);
		dialog.setTitle("Album Selection");
		dialog.setHeaderText("Please select the album to copy the photo to: ");
		dialog.setContentText("Album: ");
		
		Optional<String> result = dialog.showAndWait();
		
		int tarAl = alNames.indexOf(result.get());
		Photo selected = userAlb.get(alPos).getPhotos().get(slideshowPos);
		userAlb.get(tarAl).getPhotos().add(selected);
		userAlb.get(alPos).getPhotos().remove(selected);
		
		writeData(userAlb);
		update();
		updateManual();
	}
	/**
	 * Updates display
	 */
	public void update() {
		try {
		photoGrid.getChildren().clear();
		ArrayList<Photo> alPhotos = userAlb.get(alPos).getPhotos();
		int size = userAlb.get(alPos).getPhotos().size();
		int rows = (int) Math.ceil(((double) size) / 2);
		int count = 0;
		
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < 2; j++) {
			
			ImageView temp = new ImageView(alPhotos.get(count).getImage());
			Label tempL = new Label(alPhotos.get(count).getCaption());
			temp.setFitHeight(108);
			temp.setFitWidth(115);
			temp.setTranslateX(8);
			
			tempL.setAlignment(Pos.BOTTOM_CENTER);
			GridPane.setValignment(tempL, VPos.BOTTOM);
			GridPane.setHalignment(tempL, HPos.CENTER);
			
			photoGrid.add(temp, j, i);
			photoGrid.add(tempL, j, i);
			
			
			count++;
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Handles arrows for manual slideshow
	 * @param event
	 */
	public void handleArrows(Event event) {
		
		Button source = (Button) event.getSource();
		if(source.getId().equals("leftArrow")) {
			if(slideshowPos == 0) {
				Alert a = new Alert(AlertType.ERROR, "Invalid option, please try again.");
				a.showAndWait();
				return;
			} else {
				slideshowPos--;
				updateManual();
			}
		} else {
			if(slideshowPos == userAlb.get(alPos).getPhotoNum() - 1) {
				Alert a = new Alert(AlertType.ERROR, "Invalid option, please try again.");
				a.showAndWait();
				return;
			} else {
				slideshowPos++;
				updateManual();
			}
		}
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm MM-dd-yy");
		dateText.setText(formatter.format(userAlb.get(alPos).getPhotos().get(slideshowPos).getDate()));
	}
	/**
	 * Handles search functionality
	 */
	public void handleGoButton() {
		
	}
	/**
	 * Handles caption updating
	 */
	public void handleCaption() {
		userAlb.get(alPos).getPhotos().get(slideshowPos).setCaption(captionField.getText());
		writeData(userAlb);
		update();
	}
	/**
	 * Updates the manual photo view
	 */
	private void updateManual() {
		Photo selected = userAlb.get(alPos).getPhotos().get(slideshowPos);
		imagePreview.setImage(selected.getImage());
		captionField.setText(selected.getCaption());
	}
	/**
	 * Writes data to datafile
	 * @param o
	 */
	private void writeData(Object o) {
		try {
			FileOutputStream fOS = new FileOutputStream(dataPath);
			ObjectOutputStream oOS = new ObjectOutputStream(fOS);
			
			oOS.writeObject(o);
			
			fOS.close();
			oOS.close();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
	/**
	 * Handles deleting photos from an album
	 * @param event
	 */
	public void handleDelete(Event event) {
		Alert a = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete this Picture? This cannot be undone.", ButtonType.YES, ButtonType.NO);
    	
		
    	ButtonType result = a.showAndWait().orElse(ButtonType.NO);
    	if(ButtonType.YES.equals(result)) {
		
		userAlb.get(alPos).getPhotos().remove(slideshowPos);
		slideshowPos--;
		writeData(userAlb);
		update();
		updateManual();
    	}
	}
	/**
	 * Handles when quit button is clicked
	 * @param event
	 */
	public void handleQuitButton(Event event) {
		Platform.exit();
	}
	
	/**
	 * Method that handles log out button
	 * @param e
	 */
	public void handleLogOut(Event e) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/loginFXML.fxml"));
			Parent parent = (Parent) loader.load();
			LoginController controller = loader.<LoginController>getController();
			Scene scene = new Scene(parent);
			Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			controller.start(stage);
			stage.setScene(scene);
			stage.show();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
}
