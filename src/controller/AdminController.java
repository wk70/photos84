package controller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
/**
 * @author Matt Irving & Will Knox
 */
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;

import model.*;

public class AdminController {
	@FXML
	private Button LogoutButton;
	@FXML
	private Button QuitButton;
	
	@FXML
	private TextField usernameField;
	@FXML
	private Button CreateUserButton;
	@FXML
	private Button DeleteUserButton;
	
	@FXML
	public ListView<String> UserList;
	
	// used as a backend copy of the userlist
	private ArrayList<User> masterList = new ArrayList<User>();
	
	private final String path = "src\\data\\users.dat";
	
	
	/**
	 * Initializes Admin view
	 * @param users List of users to be initialized on UserList 
	 */
	public void start(ArrayList<User> users) {
		try {
			
			FileInputStream fIS = new FileInputStream(path);
			ObjectInputStream oIS = new ObjectInputStream(fIS);
			
			ArrayList<User> readUsers = (ArrayList<User>) oIS.readObject();
			
			masterList.clear();
			
			ArrayList<String> temp = new ArrayList<String>();
			for(User u : readUsers)
			{
				temp.add(u.getUsername());
				masterList.add(u);
			}
			
			UserList.setItems(FXCollections.observableArrayList(temp));
			UserList.getSelectionModel().select(0);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			//UserList.setPlaceholder(new Label("No content in list."));
		}
	}
	/**
	 * Method that handles Create User button
	 * @param e Event
	 */
	public void handleCreateUser(Event e) {
		boolean isDuplicate = false;
		String newUser = usernameField.getText();
		for(User user : masterList) {
			if(newUser.equals(user.getUsername()) || newUser.equals(null) || newUser.equals(""))
				isDuplicate = true;
		}
		if(isDuplicate) {
			Alert a = new Alert(AlertType.ERROR);
			a.setHeaderText("Invalid Username");
			a.setTitle("Error adding user");
			a.setContentText("This username matches a prexisting entry. Please try again.");
			a.showAndWait();
		} else {
			User tempUser = new User(newUser);
			
			masterList.add(tempUser);
			updateList();
			String userPath = "src\\data\\userData\\" + newUser + ".dat";
			File newUserFile = new File(userPath);
			if(!newUserFile.exists() || !newUserFile.isFile()) {
				try {
					newUserFile.createNewFile();
				} catch (Exception error) {
					error.printStackTrace();
				}
			}
			
		}
		
	}
	
	public void handleRemoveUser(Event e) {
		if(UserList.getSelectionModel().getSelectedItem().equals("stock")) {
			Alert b = new Alert(AlertType.ERROR, "You cannot delete the stock account.");
			b.showAndWait();
			return;
		}
		
		Alert a = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete this User? This cannot be undone.", ButtonType.YES, ButtonType.NO);
    	
		
    	ButtonType result = a.showAndWait().orElse(ButtonType.NO);
    	if(ButtonType.YES.equals(result)) {
    		
    		
    		
    		int currIndex = UserList.getSelectionModel().getSelectedIndex();
    		
    		
    		String userPath = "src\\data\\userData\\" + masterList.get(currIndex).getUsername() + ".dat";
			File newUserFile = new File(userPath);
			if(newUserFile.exists() || newUserFile.isFile()) {
				try {
					newUserFile.delete();
				} catch (Exception error) {
					error.printStackTrace();
				}
			}
			masterList.remove(currIndex);
    		updateList();
    	}
	}
	/**
	 * Method that handles log out button
	 * @param e
	 */
	public void handleLogOut(Event e) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/loginFXML.fxml"));
			Parent parent = (Parent) loader.load();
			LoginController controller = loader.<LoginController>getController();
			Scene scene = new Scene(parent);
			Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			controller.start(stage);
			stage.setScene(scene);
			stage.show();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * Method that updates ListView in application
	 */
	public void updateList() {
		ArrayList<String> temp = new ArrayList<String>();
		for(User u : masterList)
		{
			temp.add(u.getUsername());
		}
		UserList.setItems(FXCollections.observableArrayList(temp));
		writeData(masterList);
	}
	/**
	 * Handle quit button
	 * @param e
	 */
	public void handleQuit(Event e) {
		Platform.exit();
	}
	
	private void writeData(Object o) {
		try {
			FileOutputStream fOS = new FileOutputStream(path);
			ObjectOutputStream oOS = new ObjectOutputStream(fOS);
			
			oOS.writeObject(o);
			
			fOS.close();
			oOS.close();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
	
}

