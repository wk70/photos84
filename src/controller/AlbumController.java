package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import model.*;

public class AlbumController {
	
	@FXML
	private Button logoutButton;
	@FXML
	private Button quitButton;
	
	@FXML
	private ListView albumList;
	
	@FXML
	private Text albumNameText;
	@FXML
	private Text earlyDate;
	@FXML
	private Text toDate;
	@FXML
	private Text numberOfPhotos;
	
	@FXML
	private Button openButton;
	@FXML
	private Button deleteButton;
	
	@FXML
	private TextField albumNameField;
	@FXML
	private Button createButton;
	@FXML
	private Button renameButton;
	
	private User user;
	
	ArrayList<Album> userAlb = new ArrayList<Album>();
	private String dataPath; 
	/**
	 * Start scene method
	 * @param stage
	 */
	public void start(User user) {
		this.user = user;
		
		dataPath = "src\\data\\userData\\" + user.getUsername() + ".dat";
			try {
			
			FileInputStream fIS = new FileInputStream(dataPath);
			ObjectInputStream oIS = new ObjectInputStream(fIS);
			
			userAlb = (ArrayList<Album>) oIS.readObject();
			
			
			
			fIS.close();
			oIS.close();
			
			albumList.getSelectionModel().select(0);
			update();
			
			
		} catch (Exception e) {
			e.printStackTrace();
			//UserList.setPlaceholder(new Label("No content in list."));
		}
	}
	
	/**
	 * Handles album creation
	 * @param event
	 */
	public void handleCreate(Event event) {
		Album temp = new Album(albumNameField.getText());
		
		
		if(temp.getName().equals(null) || temp.getName().equals("") || checkAlbumDuplicate(temp.getName())) {
			Alert a = new Alert(AlertType.ERROR);
			a.setContentText("Invalid album name, please try again.");
			a.setHeaderText("Album name input error");
			a.showAndWait();
		} else {
		userAlb.add(temp);
		writeData(userAlb);
		update();
		}
	}
	
	public void handleDelete(Event event) {
		
		Alert a = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete this Album? This cannot be undone.", ButtonType.YES, ButtonType.NO);
    	
		
    	ButtonType result = a.showAndWait().orElse(ButtonType.NO);
    	if(ButtonType.YES.equals(result)) {
		
		int selected = albumList.getSelectionModel().getSelectedIndex();
		if(userAlb.get(selected).equals(new Album("Search"))) {
			Alert b = new Alert(AlertType.ERROR, "You cannot delete the 'Search' album.");
			b.showAndWait();
			return;
		}
		userAlb.remove(selected);
		writeData(userAlb);
		update();
    	}
	}
	/**
	 * Handles album renaming
	 * @param event
	 */
	public void handleRename(Event event) {
		int selected = albumList.getSelectionModel().getSelectedIndex();
		String newName = albumNameField.getText();
		
		
		if(newName.equals(null) || newName.equals("") || checkAlbumDuplicate(newName)) {
			Alert a = new Alert(AlertType.ERROR);
			a.setContentText("Invalid album name, please try again.");
			a.setHeaderText("Album name input error");
			a.showAndWait();
		} else {
		userAlb.get(selected).setName(newName);
		writeData(userAlb);
		update();
		}
	}
	/**
	 * Handles list selections and updates fields
	 * @param event
	 */
	public void handleListSelection(Event event) {
		int selected = albumList.getSelectionModel().getSelectedIndex();
		Album selectedAlb = userAlb.get(selected);
		
		numberOfPhotos.setText(Integer.toString(selectedAlb.getPhotoNum()));
		albumNameText.setText(selectedAlb.getName());
		
		
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm MM-dd-yy");
		if(selectedAlb.getPhotoNum() == 0) {
			earlyDate.setText("N/A");
			toDate.setText("N/A");
		} else {
			Date[] range = selectedAlb.dateRange();
			earlyDate.setText(formatter.format(range[0]));
			toDate.setText(formatter.format(range[1]));
		}
		
	}
	/**
	 * Handles when quit button is clicked
	 * @param event
	 */
	public void handleQuit(Event event) {
		Platform.exit();
	}
	/**
	 * Updates list display
	 */
	public void update() {
		ArrayList<String> temp = new ArrayList<String>();
		for(Album a : userAlb) {
			temp.add(a.getName());
		}
		
		
		albumList.setItems(FXCollections.observableArrayList(temp));
	}
	
	public void handleOpen(Event e) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/photosFXML.fxml"));
			Parent parent = (Parent) loader.load();
			PhotoController controller = loader.<PhotoController>getController();
			Scene scene = new Scene(parent);
			Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			controller.start(user, userAlb.get(albumList.getSelectionModel().getSelectedIndex()));
			stage.setScene(scene);
			stage.show();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
	/**
	 * Method that handles log out button
	 * @param e
	 */
	public void handleLogOut(Event e) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/loginFXML.fxml"));
			Parent parent = (Parent) loader.load();
			LoginController controller = loader.<LoginController>getController();
			Scene scene = new Scene(parent);
			Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			controller.start(stage);
			stage.setScene(scene);
			stage.show();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	/**
	 * Writes data to datafile
	 * @param o
	 */
	private void writeData(Object o) {
		try {
			FileOutputStream fOS = new FileOutputStream(dataPath);
			ObjectOutputStream oOS = new ObjectOutputStream(fOS);
			
			oOS.writeObject(o);
			
			fOS.close();
			oOS.close();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
	
	
	/**
	 * Checks to see if input {@code str} is a duplicate of an existing album's name
	 * @param str Desired name to check for duplicates
	 */
	private boolean checkAlbumDuplicate(String str) {
		boolean duplicate = false;
		for(Album a : userAlb) {
			if(str.equals(a.getName()))
				duplicate = true;
		}
		return duplicate;
	}
	/**
	 * Method that checks whether or not a given string is numeric
	 * @param strNum String to check
	 * @return {@code true} if string is an int, {@code false} if it is null or throws a NumberFormatException error
	 */
	public boolean isNumeric(String strNum) {
		if (strNum == null)
			return false;
		try {
			int i = Integer.parseInt(strNum);
		} catch (NumberFormatException err) {
			return false;
		}
		return true;
	}
	
	
}
