package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import model.*;

/**
 * 
 * @author Matt Irving & Will Knox
 *
 */
public class LoginController {
	@FXML
	private Button loginButton;
	@FXML
	private Button quitButton;
	@FXML
	private TextField usernameField;
	
	private final String path = "src\\data\\users.dat";
	
	ArrayList<User> users;
	
	AdminController adminController;
	/**
	 * Default start method for main stage
	 * @param stage
	 */
	public void start(Stage stage) {
		
	}
	/**
	 * Handles when login button is clicked
	 * @param event
	 */
	@SuppressWarnings("unchecked")
	public void handleLoginButton(Event event) {
		String username = usernameField.getText();

		File data = new File(path);
		
		
		if(!data.exists() || !data.isFile()) {
			try {
				data.createNewFile();
				Album stock = new Album("Stock");
				String stockPath = "src\\data\\stockData\\";
				File photoFile;
				
				User stockUser = new User("stock");
				stockUser.getAlbums().add(stock);
				users = new ArrayList<User>();
				users.add(stockUser);
				
				try {
					FileOutputStream fOS = new FileOutputStream(path);
					ObjectOutputStream oOS = new ObjectOutputStream(fOS);
				
					oOS.writeObject(users);
					
					oOS.close();
					fOS.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try {
			FileInputStream fIS = new FileInputStream(path);
			ObjectInputStream oIS = new ObjectInputStream(fIS);
			users = (ArrayList<User>) oIS.readObject();
			
			oIS.close();
			fIS.close();
			
			User user = null;
			
			for(User currUser : users) {
				if(currUser.getUsername().equals(username))
					user = currUser;
			}
			
			if(username.equals("admin") || user != null) {
				FXMLLoader loader;
				Parent parent;
				
				if(username.equals("admin")) {
					loader = new FXMLLoader(getClass().getResource("/view/adminFXML.fxml"));
					parent = (Parent) loader.load();
					adminController = loader.<AdminController>getController();
					Scene scene = new Scene(parent);
					Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
					adminController.start(users);
					stage.setScene(scene);
					stage.show();
				} else {
					loader = new FXMLLoader(getClass().getResource("/view/albumsFXML.fxml"));
					parent = (Parent) loader.load();
					AlbumController controller = loader.<AlbumController>getController();
					Scene scene = new Scene(parent);
					Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
					controller.start(user);
					stage.setScene(scene);
					stage.show();
				}
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Login Error");
				alert.setHeaderText("Invalid username.");
				alert.setContentText("The specified user does not exist.");
				
				alert.showAndWait();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Handles when quit button is clicked
	 * @param event
	 */
	public void handleQuitButton(Event event) {
		Platform.exit();
	}
}
